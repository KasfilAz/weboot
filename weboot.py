import argparse

from Scripts.browse import Browse

parser = argparse.ArgumentParser(description="Auto visit and click bot")
parser.add_argument(
    "command",
    type=str,
    default="start",
    help="things you wanna do [start|check] default = start",
)

args = parser.parse_args()
command = args.command

if command == "start":
    browse = Browse()
    browse.starting()
