"""
Dirpath.py

File contain directory and path function
declaration of elements for this app
"""
import datetime


from os import getenv
from os.path import dirname
from dotenv import load_dotenv


class Dirpath:
    """
    Dirpath class

    Handling all directory path and path of element
    """

    def __init__(self):
        self.root_dir = dirname(dirname(__file__))
        env_path = self.env_path()
        load_dotenv(dotenv_path=env_path)

    def proxies_dir(self):
        """ Getting proxies directory """
        return self.root_dir + "/" + getenv('PROXIES_DIR')

    def driver_path(self):
        """ Getting webDriver Path for selenium """
        webdriver_dir = getenv('WEBDRIVER_DIR') + "/"
        webdriver_name = getenv('WEBDRIVER_NAME')
        return self.root_dir + "/" + webdriver_dir + webdriver_name

    def ext_dir(self):
        """ Getting extension Directory """
        ext_dir = getenv('EXTENSIONS_DIR')
        return self.root_dir + "/" + ext_dir

    def env_path(self):
        """ Get env path """
        root_dir = self.root_dir
        return root_dir + "/.env"

    def init_db_reports(self):
        """ Get database path """
        now = datetime.datetime.now()
        db_dir = getenv('STORAGES_DIR')
        db_name = "reports_" + now.strftime("%b_%d_%y") + ".db"
        db_path = self.root_dir + "/" + db_dir + "/" + db_name

        return db_path
