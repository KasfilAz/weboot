"""
Proxies.py

This file contain all class that handle function for
managing proxies.
"""
from __future__ import absolute_import
from os import scandir
from Scripts.dirpath import Dirpath


class Proxies:
    """
    Proxies Class

    Handle all function for proxy processing
    """

    def __init__(self):
        dirpath = Dirpath()
        self.proxies_dir = dirpath.proxies_dir()

    def get_all_proxies_file(self):
        """ get all proxy from all file """
        # Variable for holding proxy files
        proxy_files = []
        files = scandir(self.proxies_dir)

        for file in files:
            proxy_files.append(self.proxies_dir + "/" + file.name)

        return proxy_files

    def get_proxy_lists(self):
        """ Get and Convert proxies from files """

        files = self.get_all_proxies_file()
        proxies = []

        for file in files:
            # Loading file
            loaded_file = open(file, 'r')
            # Loop trough all lines inside file
            for line in loaded_file:
                # Converting line to [ip address, port]
                line = line.strip().split(':')

                ip_addr = line[0]
                port = int(line[1])
                proxy = [ip_addr, port]
                # Appending converted line to proxies
                proxies.append(proxy)

        return proxies
