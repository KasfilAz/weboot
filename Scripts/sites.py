"""
Sites.py

File that define sites data and ads xpath
"""
SITES_DATA = [
    {
        "url": "http://kasfaz.000webhostapp.com",
        "xpath": []
    },
    {
        "url": "http://programmer-menulis.blogspot.com",
        "xpath": [
            '//*[@id="HTML1"]/div/iframe',
            '//*[@id="adpays.net_unit_728x90"]/iframe',
            '//*[@id="HTML8"]/div[1]/iframe',
            '//*[@id="HTML7"]/div[1]/iframe'
        ]
    },
    # {
    #     "url": "http://mazsupertop.blogspot.com",
    #     "xpath": [
    #         '//*[@id="adpays.net_unit_728x90"]/iframe',
    #         '//*[@id="HTML2"]/div[1]/iframe',
    #         '//*[@id="HTML10"]/div[1]/iframe',
    #         '//*[@id="adpays.net_unit_300x250"]/iframe',
    #         '//*[@id="adpays.net_unit_160x600"]/iframe'
    #     ]
    # }
]
