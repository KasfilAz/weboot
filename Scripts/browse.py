"""
Browser.py

File contain browse class that handle all
browse with selenium function.
"""
import os
import time
import random
import multiprocessing as mp

from dotenv import load_dotenv
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException

from Scripts.proxies import Proxies
from Scripts.dirpath import Dirpath
from Scripts.sites import SITES_DATA


class Browse:
    """
    Browse class

    handle all Browsing and it's behavior's function
    """

    def __init__(self):
        proxy = Proxies()
        dirpath = Dirpath()

        env_path = dirpath.env_path()
        load_dotenv(dotenv_path=env_path)

        self.proxies = proxy.get_proxy_lists()
        self.process_count = os.getenv('PROCESS_COUNT')
        self.ext_dir = dirpath.ext_dir()

    def get_proxies(self):
        """ Get all proxies ready for being used """
        proxies = self.proxies
        for proxy in proxies:
            print("ip: %s - port: %i" % (proxy[0], proxy[1]))

    def get_all_ext(self):
        """ get all extensions inside dir """
        exts = os.scandir(self.ext_dir)
        extensions = []

        for ext in exts:
            extensions.append(self.ext_dir + "/" + ext)

        return extensions

    @classmethod
    def set_preference(cls, ip_addr, port):
        """ Set preference for browser """
        profile = webdriver.FirefoxProfile()
        socks_version = int(os.getenv('SOCKS_VERSION'))
        print("ip: %s - port: %i" % (ip_addr, port))

        profile.set_preference('network.proxy.type', 1)
        profile.set_preference('network.proxy.socks', ip_addr)
        profile.set_preference('network.proxy.socks_port', port)
        profile.set_preference('network.proxy.socks_version', socks_version)

        return profile

    def add_extension(self, browser):
        """ Adding all extensions to browser """
        extensions_dir = self.ext_dir
        extensions = os.scandir(extensions_dir)
        for extension in extensions:
            browser.install_addon(
                extensions_dir + "/" + extension.name, temporary=True)

        return browser

    @classmethod
    def browser_scroll(cls, browser):
        """ Perform scrolling on browser """
        i = 0
        # scroll down 4 times
        while i < 3:
            browser.find_element_by_tag_name('html').send_keys(Keys.PAGE_DOWN)
            time.sleep(1)
            i += 1

        i = 0
        # then scroll up 4 times
        while i < 3:
            browser.find_element_by_tag_name('html').send_keys(Keys.PAGE_UP)
            time.sleep(1)
            i += 1

    def browse(self, proxy):
        """ Instantiate browser and start browsing """
        ip_addr = proxy[0]
        port = proxy[1]

        options = Options()
        options.headless = True
        profile = self.set_preference(ip_addr, port)
        sites = SITES_DATA
        browser = webdriver.Firefox(firefox_profile=profile, options=options)
        browser = self.add_extension(browser)

        for i in enumerate(sites):
            site = dict(i[1])
            try:
                browser.get(site.get('url'))
                xpath = site.get('xpath')
                try:
                    WebDriverWait(browser, 3).until(
                        EC.visibility_of_element_located((By.TAG_NAME, 'body')))
                    self.browser_scroll(browser)
                    time.sleep(15)
                    self.ads_click(browser, xpath)
                except TimeoutException:
                    print('cannot scrolling')

                time.sleep(random.randint(30, 60))

            except WebDriverException:
                break

        browser.quit()

    def starting(self):
        """ Start Browsing site """
        proxies = self.proxies
        process_count = int(os.getenv("PROCESS_COUNT"))

        with mp.Pool(processes=process_count) as pool:
            pool.map(self.browse, proxies)

    @classmethod
    def ads_click(cls, browser, xpath):
        """ Clicking ads from path """
        # loop trough all xpath
        for path in xpath:
            try:
                frame = browser.find_element_by_xpath(path)
                browser.switch_to.frame(frame)

                try:
                    ads = browser.find_element_by_tag_name("iframe")
                    browser.switch_to.frame(ads)
                    print("swithc to another iframe")
                    link = browser.find_element_by_tag_name("body")
                    link.click()
                    print("ads Clicked!")
                    time.sleep(10)
                except NoSuchElementException:
                    link = browser.find_element_by_tag_name("body")
                    link.click()
                    print("ads Clicked!")
                    time.sleep(10)

                browser.switch_to.default_content()
            except NoSuchElementException:
                browser.switch_to.default_content()
                print("Ads not loaded")
                continue
            except ElementNotInteractableException as err:
                browser.switch_to.default_content()
                print(err.msg)
                continue
